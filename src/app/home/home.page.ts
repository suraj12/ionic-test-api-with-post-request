import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  tests: any;

  constructor(
    private http: HttpClient
  ) {}

  showData() {
      this.http.post('https://carjuggle.ca/api/v1/vehicle/search/', { selected_option: 'buy' }, { headers: new HttpHeaders({
        'content-type': 'application/json',
        Authorization: 'Basic ' + btoa('demouser.carjuggle:gjw7q28)p9$&v-fj0xyh&d8i)hs@+omj#(b!+dv^biy!lg(2da'),
      'Access-Control-Allow-Origin': '*'
      })
    }).subscribe(response => {
        this.tests = response;
        console.log(this.tests);
      }, error => {
        console.log(error);
      });
    }


}
